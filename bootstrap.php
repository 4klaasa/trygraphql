<?php

require_once __DIR__ . '/vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Managers\GoodsManager;

$paths = [__DIR__ . "/src/Entities"];
$dbParams = include __DIR__ . '/config/db_config.php';
$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;

$config = Setup::createAnnotationMetadataConfiguration(
    $paths,
    $isDevMode,
    $proxyDir,
    $cache,
    $useSimpleAnnotationReader
);

GoodsManager::getInstance()->setEntityManager(
    EntityManager::create($dbParams, $config)
);
