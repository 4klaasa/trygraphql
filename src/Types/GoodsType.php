<?php


namespace Types;

use Entities\Goods;
use Entities\GoodsSpecification;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use Managers\GoodsManager;
use Exception;

class GoodsType extends ObjectType
{

    public function __construct()
    {
        $config = [
            'description' => 'Товар',
            'fields' => [
                'id' => [
                    'type' => TypesRegistry::id(),
                    'description' => 'Идентификатор товара'
                ],
                'name' => [
                    'type' => TypesRegistry::string(),
                    'description' => 'Наименование товара'
                ],
                'price' => [
                    'type' => TypesRegistry::float(),
                    'description' => 'Цена товара'
                ],
                'description' => [
                    'type' => TypesRegistry::string(),
                    'description' => 'Описание товара'
                ],
                'specification' => [
                    'type' => TypesRegistry::listOf(TypesRegistry::specification()),
                    'description' => 'Характеристики товара'
                ],
            ],
            'resolveField' => function (Goods $goods, $args, $context, ResolveInfo $info) {
                return $this->resolveField($goods, $info->fieldName);
            }
        ];
        parent::__construct($config);
    }

    /**
     * @param Goods $goods
     * @param string $field
     * @return mixed
     * @throws Exception
     */
    protected function resolveField(Goods $goods, $field)
    {
        $resolver = 'resolve' . ucfirst($field);
        if (method_exists($this, $resolver)) {
            return $this->$resolver($goods);
        }

        $getter = 'get' . ucfirst($field);
        if (method_exists($goods, $getter)) {
            return $goods->$getter();
        }

        throw new Exception(__CLASS__ . ": Не известное св-во {$field}");
    }

    /**
     * @param Goods $goods
     * @return GoodsSpecification[]
     */
    protected function resolveSpecification(Goods $goods)
    {
        return GoodsManager::getInstance()->getGoodsSpecification($goods->getId());
    }

}