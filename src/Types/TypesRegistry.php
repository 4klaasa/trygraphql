<?php


namespace Types;

use GraphQL\Type\Definition\BooleanType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\FloatType;
use GraphQL\Type\Definition\IDType;
use GraphQL\Type\Definition\StringType;

class TypesRegistry
{

    private static $goods;
    private static $query;
    private static $specification;
    private static $inputGoods;
    private static $inputSpecification;

    public static function inputSpecification()
    {
        return self::$inputSpecification ?: (self::$inputSpecification = new InputSpecificationType());
    }

    public static function inputGoods()
    {
        return self::$inputGoods ?: (self::$inputGoods = new InputGoodsType());
    }

    public static function nonNull($type)
    {
        return Type::nonNull($type);
    }

    public static function listOf($type)
    {
        return Type::listOf($type);
    }

    /**
     * @return GoodsType
     */
    public static function specification(): SpecificationType
    {
        return self::$specification ?: (self::$specification = new SpecificationType());
    }

    /**
     * @return GoodsType
     */
    public static function goods(): GoodsType
    {
        return self::$goods ?: (self::$goods = new GoodsType());
    }

    /**
     * @return QueryType
     */
    public static function query(): QueryType
    {
        return self::$query ?: (self::$query = new QueryType());
    }

    /**
     * @return StringType
     */
    public static function string(): StringType
    {
        return Type::string();
    }

    /**
     * @return FloatType
     */
    public static function float(): FloatType
    {
        return Type::float();
    }

    /**
     * @return IDType
     */
    public static function id(): IDType
    {
        return Type::id();
    }

    /**
     * @return IntType
     */
    public static function int(): IntType
    {
        return Type::int();
    }

    /**
     * @return BooleanType
     */
    public static function boolean(): BooleanType
    {
        return Type::boolean();
    }

}