<?php


namespace Types;

use Entities\GoodsSpecification;
use Exception;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;

class SpecificationType extends ObjectType
{

    public function __construct()
    {
        $config = [
            'description' => 'Характеристика товара',
            'fields' => [
                'id' => [
                    'type' => TypesRegistry::id(),
                    'description' => 'Идентификатор характеристики'
                ],
                'name' => [
                    'type' => TypesRegistry::string(),
                    'description' => 'Наименование характеристики'
                ],
                'value' => [
                    'type' => TypesRegistry::string(),
                    'description' => 'Значение характеристики'
                ],
            ],
            'resolveField' => function ($spec, $args, $context, ResolveInfo $info) {
                return $this->resolveField($spec, $info->fieldName);
            }
        ];
        parent::__construct($config);
    }

    /**
     * @param GoodsSpecification $spec
     * @param string $field
     * @return mixed
     * @throws Exception
     */
    protected function resolveField(GoodsSpecification $spec, $field)
    {
        $getter = 'get' . ucfirst($field);
        if (method_exists($spec, $getter)) {
            return $spec->$getter();
        }

        throw new Exception(__CLASS__ . ": Не известное св-во {$field}");
    }
}