<?php


namespace Types;


use GraphQL\Type\Definition\InputObjectType;

class InputGoodsType extends InputObjectType
{
    public function __construct()
    {
        $config = [
            'description' => 'Добавление товара',
            'fields' => [
                'name' => [
                    'type' => TypesRegistry::nonNull(TypesRegistry::string()),
                    'description' => 'Наименование товара'
                ],
                'price' => [
                    'type' => TypesRegistry::nonNull(TypesRegistry::float()),
                    'description' => 'Цена товара'
                ],
                'description' => [
                    'type' => TypesRegistry::string(),
                    'description' => 'Описание товара'
                ],
                'specification' => [
                    'type' => TypesRegistry::listOf(TypesRegistry::inputSpecification()),
                    'description' => 'Характеристики товара'
                ],
            ]
        ];
        parent::__construct($config);
    }
}