<?php


namespace Types;

use GraphQL\Type\Definition\InputObjectType;


class InputSpecificationType extends InputObjectType
{

    public function __construct()
    {
        $config = [
            'description' => 'Добавление характеристики товара',
            'fields' => [
                'name' => [
                    'type' => TypesRegistry::string(),
                    'description' => 'Наименование характеристики'
                ],
                'value' => [
                    'type' => TypesRegistry::string(),
                    'description' => 'Значение характеристики'
                ],
            ]
        ];
        parent::__construct($config);
    }
}

