<?php


namespace Types;


use Entities\Goods;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use Managers\GoodsManager;
use Doctrine\ORM as ORM;

class MutationType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => [
                'addGoods' => [
                    'type' => TypesRegistry::goods(),
                    'description' => 'Добавление товара',
                    'args' => [
                        'goods' => TypesRegistry::nonNull(TypesRegistry::inputGoods())
                    ]
                ],
                'updateGoods' => [
                    'type' => TypesRegistry::goods(),
                    'description' => 'Обновление товара',
                    'args' => [
                        'id' => TypesRegistry::nonNull(TypesRegistry::id()),
                        'goods' => TypesRegistry::nonNull(TypesRegistry::inputGoods())
                    ]
                ],
                'deleteGoods' => [
                    'type' => TypesRegistry::boolean(),
                    'description' => 'Удаление товара',
                    'args' => [
                        'id' => TypesRegistry::nonNull(TypesRegistry::id()),
                    ]
                ]
            ],
            'resolveField' => function ($val, $args, $context, ResolveInfo $info) {
                return $this->{$info->fieldName}($args);
            }
        ];
        parent::__construct($config);
    }

    /**
     * @param $args
     * @return Goods
     * @throws ORM\ORMException
     * @throws ORM\OptimisticLockException
     */
    protected function addGoods(array $args): Goods
    {
        return GoodsManager::getInstance()->create($args['goods']);
    }

    /**
     * @param array $args
     * @return Goods
     * @throws ORM\ORMException
     * @throws ORM\OptimisticLockException
     */
    protected function updateGoods(array $args): Goods
    {
        return GoodsManager::getInstance()->update(
            $args['id'],
            $args['goods']
        );
    }

    /**
     * @param array $args
     * @return boolean
     * @throws ORM\ORMException
     * @throws ORM\OptimisticLockException
     */
    protected function deleteGoods($args): bool
    {
        GoodsManager::getInstance()->delete($args['id']);
        return true;
    }

}