<?php


namespace Types;

use Entities\Goods;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use Managers\GoodsManager;


class QueryType extends ObjectType
{

    public function __construct()
    {
        $config = [
            'fields' => [
                'goodsById' => [
                    'type' => TypesRegistry::goods(),
                    'description' => 'Возвращает товар по id',
                    'args' => [
                        'id' => [
                            'type' => TypesRegistry::int(),
                            'description' => 'Идентификатор товара'
                        ]
                    ]
                ],
                'allGoods' => [
                    'type' => TypesRegistry::listOf(TypesRegistry::goods()),
                    'description' => 'Список всех товаров',
                ]
            ],
            'resolveField' => function ($val, $args, $context, ResolveInfo $info) {
                return $this->{$info->fieldName}($args);
            }
        ];

        parent::__construct($config);
    }

    protected function goodsById($args): Goods
    {
        return GoodsManager::getInstance()->find($args['id']);
    }

    /**
     * @return Goods[]
     */
    protected function allGoods(): array
    {
        return GoodsManager::getInstance()->getAll();
    }

}