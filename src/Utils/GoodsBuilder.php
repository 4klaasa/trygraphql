<?php


namespace Utils;


use Doctrine\Common\Collections\ArrayCollection;
use Entities\Goods;
use Entities\GoodsSpecification;

class GoodsBuilder
{
    /**
     * @param Goods $goodsEntity
     * @param array $props
     * @return Goods
     */
    public function buildFromArray(Goods $goodsEntity, array $props = []): Goods
    {
        if (isset($props['specification'])) {
            $goodsEntity->setSpecification(new ArrayCollection());
            foreach ($props['specification'] as $spec) {
                $specificationEntity = (new GoodsSpecification())
                    ->setValue($spec['value'])
                    ->setName($spec['name']);
                $goodsEntity->addSpecification($specificationEntity);
            }
        }

        $goodsEntity->setName($props['name'])
            ->setDescription($props['description'])
            ->setPrice($props['price']);

        return $goodsEntity;

    }

}