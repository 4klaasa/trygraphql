<?php


namespace Managers;


use Entities\Goods;
use Doctrine\ORM as ORM;
use Entities\GoodsSpecification;
use Repositories\GoodsRepository;
use Repositories\SpecificationRepository;
use Utils\GoodsBuilder;


class GoodsManager
{
    private static $instance = null;

    /**
     * @var ORM\EntityManager
     */
    private $em;

    private function __clone()
    {
    }

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return GoodsRepository
     */
    private function getGoodsRepository(): GoodsRepository
    {
        /**
         * @var $repository GoodsRepository
         */
        $repository = $this->em->getRepository(Goods::class);
        return $repository;
    }

    /**
     * @return SpecificationRepository
     */
    private function getSpecificationRepository(): SpecificationRepository
    {
        /**
         * @var $repository SpecificationRepository
         */
        $repository = $this->em->getRepository(GoodsSpecification::class);
        return $repository;
    }

    public function setEntityManager(ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    protected function getEntityManager(): ORM\EntityManager
    {
        return $this->em;
    }

    /**
     * @param array $data
     * @return Goods
     * @throws ORM\ORMException
     * @throws ORM\OptimisticLockException
     */
    public function create(array $data): Goods
    {
        $goods = new Goods();
        (new GoodsBuilder())->buildFromArray($goods, $data);
        $this->em->persist($goods);
        $this->em->flush();
        return $goods;
    }

    /**
     * @param int $goodsId
     * @param array $data
     * @return Goods
     * @throws ORM\ORMException
     * @throws ORM\OptimisticLockException
     */
    public function update(int $goodsId, array $data)
    {
        /**
         * @var $goods Goods
         */
        $goods = $this->em->getReference(Goods::class, $goodsId);
        (new GoodsBuilder())->buildFromArray($goods, $data);
        $this->em->persist($goods);
        $this->em->flush();
        return $goods;
    }

    /**
     * @param int $goodsId
     * @throws ORM\ORMException
     * @throws ORM\OptimisticLockException
     */
    public function delete(int $goodsId)
    {
        $goods = $this->em->getReference(Goods::class, $goodsId);
        $this->em->remove($goods);
        $this->em->flush();
    }

    /**
     * @param int $goodsId
     * @return Goods
     */
    public function find(int $goodsId): Goods
    {
        /**
         * @var $goods Goods
         */
        $goods = $this->getGoodsRepository()->find($goodsId);
        return $goods;
    }

    /**
     * @return Goods[]
     */
    public function getAll(): array
    {
        return $this->getGoodsRepository()->findAll();
    }

    /**
     * @param int $goodsId
     * @return GoodsSpecification[]
     */
    public function getGoodsSpecification(int $goodsId): array
    {
        return $this->getSpecificationRepository()->findByGoodsId($goodsId);
    }

}