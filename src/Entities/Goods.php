<?php


namespace Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Repositories\GoodsRepository")
 * @ORM\Table(name="goods")
 */
class Goods
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="GoodsSpecification", mappedBy="goods", cascade={"persist","remove"}, orphanRemoval=true)
     */
    protected $specification;


    public function __construct()
    {
        $this->specification = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Goods
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Goods
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Goods
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Goods
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSpecification(): ArrayCollection
    {
        return $this->specification;
    }

    /**
     * @param ArrayCollection $specification
     * @return Goods
     */
    public function setSpecification(ArrayCollection $specification)
    {
        $this->specification = $specification;
        return $this;
    }

    /**
     * @param GoodsSpecification $goodsSpecification
     * @return $this
     */
    public function addSpecification(GoodsSpecification $goodsSpecification)
    {
        $goodsSpecification->setGoods($this);
        $this->specification->add($goodsSpecification);
        return $this;
    }

}
