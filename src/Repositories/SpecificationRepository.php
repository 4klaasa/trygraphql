<?php


namespace Repositories;

use Doctrine\ORM\EntityRepository;
use Entities\GoodsSpecification;


class SpecificationRepository extends EntityRepository
{
    /**
     * @param int $goodsId
     * @return GoodsSpecification[]
     */
    public function findByGoodsId(int $goodsId): array
    {
        $dql = 'SELECT a FROM Entities\GoodsSpecification a WHERE a.goods = :goodsId';
        $q = $this->getEntityManager()->createQuery($dql);
        $q->setParameter('goodsId', $goodsId);

        return $q->getResult();
    }
}