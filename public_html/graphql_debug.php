<?php

require_once __DIR__ . '/../bootstrap.php';

use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use Types\QueryType;
use Types\MutationType;
use GraphQL\Error\FormattedError;
use GraphQL\Error\Debug;

$status = 200;
$result = [];
try {

    $logDir = __DIR__ . '/../logs';
    $debug = Debug::INCLUDE_DEBUG_MESSAGE | Debug::INCLUDE_TRACE;
    $rawInput = file_get_contents('php://input');
    $input = json_decode($rawInput, true);
    $query = $input['query'];

    file_put_contents($logDir . '/debug.log', print_r($query, true), FILE_APPEND);

    $schema = new Schema([
        'query' => new QueryType(),
        'mutation' => new MutationType()
    ]);

    $result = GraphQL::executeQuery($schema, $query)->toArray($debug);

    file_put_contents($logDir . '/debug.log', print_r($result, true), FILE_APPEND);

} catch (Exception $e) {

    file_put_contents($logDir . '/error.log', print_r($e, true), FILE_APPEND);

    $status = 500;
    $result = [
        'errors' => [FormattedError::createFromException($e)]
    ];
}

header('Content-Type: application/json; charset=UTF-8', true, $status);
echo json_encode($result);