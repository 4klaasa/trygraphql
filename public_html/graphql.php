<?php

require_once __DIR__ . '/../bootstrap.php';

use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use Types\QueryType;
use Types\MutationType;
use GraphQL\Error\FormattedError;

$status = 200;
try {

    $rawInput = file_get_contents('php://input');
    $input = json_decode($rawInput, true);
    $query = $input['query'];

    $schema = new Schema([
        'query' => new QueryType(),
        'mutation' => new MutationType()
    ]);

    $result = GraphQL::executeQuery($schema, $query)->toArray();

} catch (Exception $e) {
    $status = 500;
    $result = [
        'errors' => [FormattedError::createFromException($e)]
    ];
}

header('Content-Type: application/json; charset=UTF-8', true, $status);
echo json_encode($result);